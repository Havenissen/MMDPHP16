<?php
    /*
     * Opgave 03_5
     * 
     * Metoden metoden getLangAndLong skal kunne hente Lat og Long for hvert enkelt indlejret array i arrayet $events.
     * Når metoden har fundet det, skal det returneres.
     * Se kapitel - Array -> Extracting multiple values. Den indbyggede metode list() er anvenlig i den sammenhæng.
     */
    
    class Event
    {
        private $events = array(
            array(
            "EventId"=>1,
            "EventName"=>"Rave party",
            "EventDescription"=>"For young people",
            "EventDate"=>"Oktober 1 2016 10:00pm",
            "Lat"=>"56.4",
            "Long"=>"9",
            "EventImage"=>"img/rave.png"
        ),
        array(
            "EventId"=>2,
            "EventName"=>"Opera",
            "EventDescription"=>"For not so young people",
            "EventDate"=>"Oktober 2 2016 10:00pm",
            "Lat"=>"56.3",
            "Long"=>"9.4",
            "EventImage"=>"img/opera.png"
        ),
        array(
            "EventId"=>3,
            "EventName"=>"Metal",
            "EventDescription"=>"For everybody",
            "EventDate"=>"Oktober 2 2016 2:00am",
            "Lat"=>"56.4",
            "Long"=>"9.3",
            "EventImage"=>"img/metal.png"
        ));
        function __construct()
        {
        }
        function getLatAndLong()
        {
            foreach($this->events as $ev)
            {
                $keys = array_keys($ev);
                echo "$ev[Lat],$ev[Long]<br>";
            }
        }
    }
        $event = new Event;
        $event->getLatAndLong();
?>