<?php 
    //Klasse der indeholder databasevariabler, $conn og tjekker om forbindelsen er ok.
    //Klassen kan instantieres fra hvilket som helst i applikationen
    class DB
    {
        //Lokale variabler (Private betyder, at de kun kan tilgås fra denne klasse. I dette tilfælde benyttes i __construct())
        //Variablerne bruges til forbindelsen DB serveren
        //Disse informationer er 'variable' i forhold til din SQL servere og databaseskema.
        private $DB_SERVER = "localhost";
        private $DB_NAME    = "educationdb";
        private $DB_USER    = "root";
        private $DB_PASS    = "";
        private $DB_PORT    = "3307";
        

        //Denne variable gøres public, så vi fra vores andre klasser, kan tilgå den. Fx fra Story.
        public $conn;

        // Når objektet oprettes, eksekveres nedenstående metode (function). __construct() er en special metode der eksekveres,
        // når oprettet oprettes. Det giver god mening, at vi forsøger initialisere forbindelsen og ser efter som $conn returnerer 
        // nogle fejl. Konstruktøren tager ingen parametre, da alle server config data ligger i denne fil.
        function __construct() {

            // Forbindelsesvariablen - forbindelse. Det vil sige, at vi kun definere denne variabel.
            // Vi benytter $conn længere nede til at oprette forbindelsen til serveren.
            $this->conn = new mysqli( $this->DB_SERVER, $this->DB_USER, $this->DB_PASS, $this->DB_NAME );
            // Tjek om der er forbindelse
            // Her forsøger vi at oprette forbindelse. Hvis forbindelsen fejler, vil if sætningen returnere en fejlmeddelelse.
            // Hvis den ikke kommer med fejl vil den forsætte
            if ($this->conn->connect_error) {
                die("Forbindelsen fejlede : " . $this->conn->connect_error);
            }
        }
    }
?>