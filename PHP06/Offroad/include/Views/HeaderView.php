<!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Alle kan bidrage med historier fra Offroad 2017!</div>
                <div class="intro-heading">Historier fra Offroad 2017</div>
                <a href="#services" class="page-scroll btn btn-xl">Tell Me More</a>
            </div>
        </div>
    </header>