<?php
    /*
     * Opgave 03_01
     * 
     * En event er en begivendhed der er afgrænset ved tid og sted, og som er kendetegnet ved specifikt indhold.
     * 
     * Opgave 1) 
     * Find nogle flere events. Søg inspiration på http://www.visitskive.dk/skive/begivenheder-2
     * Indsæt de fundne events i $events arrayet nede i klassen Event. Find latitude og longitude på https://itouchmap.com/latlong.html
     *
     * Opgave 2)
     * I klassen Event er der en tom metode der hedder getAllEvents. Metoden skal returnere hele $events arrayet fra klassen.
     * Når vi returnerer hele arrayet, så kan vi anvende det andre steder. Fx i index.php
     *
     * Opgave 3)
     * Gå til index.php
     * Lokaliser der hvor markører bliver indsat på kortet. Erstat de to eksisterende markører med php kode. Koden skal løbe igennem det array, som metoden getAllEvents 
     * returnerer og udskrive indholdet, således, at hver gang løkken løber en gang, vises en markør på kortet.
     * 
     * 
     */
    
    class Event
    {
        private $events = array(
            array(
            "EventId"=>1,
            "EventName"=>"Rave party",
            "EventDescription"=>"For young people",
            "EventDate"=>"Oktober 1 2016 10:00pm",
            "Lat"=>"56.4",
            "Long"=>"9",
            "EventImage"=>"img/rave.png"
        ),
        array(
            "EventId"=>2,
            "EventName"=>"Opera",
            "EventDescription"=>"For not so young people",
            "EventDate"=>"Oktober 2 2016 10:00pm",
            "Lat"=>"56.3",
            "Long"=>"9.4",
            "EventImage"=>"img/opera.png"
        ),
        array(
            "EventId"=>3,
            "EventName"=>"Metal",
            "EventDescription"=>"For everybody",
            "EventDate"=>"Oktober 2 2016 2:00am",
            "Lat"=>"56.4",
            "Long"=>"9.3",
            "EventImage"=>"img/metal.png"
        ),
        array(
            "EventId"=>4,
            "EventName"=>"Sejllads",
            "EventDescription"=>"For everybody",
            "EventDate"=>"13/09/2016-17/09/2016",
            "Lat"=>"56.57",
            "Long"=>"9.0",
            "EventImage"=>"img/limfjordrundt.png"
        ),
        array(
            "EventId"=>5,
            "EventName"=>"Oyster",
            "EventDescription"=>"For everybody",
            "EventDate"=>"11/10/2016-20/10/2016",
            "Lat"=>"56.6",
            "Long"=>"9.04",
            "EventImage"=>"img/oyster.png"
        ));
        function __construct()
        {
            //Konstruktør (funktionen) skal ikke benyttes
        }
        function getAllEvents()
        {
            //Denne funktion skal returnere alle event i events arrayet
            foreach($this->events as $ev) {
                $keys = array_keys ($ev);
                echo 'L.marker([' . $ev["Lat"] . ',' .$ev["Long"] . ']).addTo(mymap).bindPopup("<b>' . $ev["EventName"] . '</b><br />' . $ev["EventDate"] . '");';
            }
        }
    }
?>