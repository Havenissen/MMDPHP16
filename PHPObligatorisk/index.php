<?php
    include("event.php");
?>
<html lang="da" dir="ltr" class="client-nojs">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.0-rc.3/dist/leaflet.css" />
        <title>Google Map</title>
        <!-- Al funktionalitet til visning af maps hentes fre denne adresse -->
        <script src="https://unpkg.com/leaflet@1.0.0-rc.3/dist/leaflet.js"></script>
        <script>
        function init(){
            //http://leafletjs.com
            
            //Vi initierer først vores map - center og zoom
            var mymap = L.map('mapBasic').setView([56.553801, 9.02201], 10);
            //Hvordan skal vores kort se ud?
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
                maxZoom: 16,
                attribution: 'Dette kort er et interaktivt kort produceret af MMD 2016',
                id: 'mapbox.comic'
            }).addTo(mymap);
            
            //Opstil markører
            <?php
            $event = new Event;
            $event->getAllEvents();
            ?>
            //Følgende funktionalitet gør det muligt, at klikke på en markør.
            var popup = L.popup();
            function onMapClick(e) {
                popup
                    .setLatLng(e.latlng)
                    .setContent("You clicked the map at " + e.latlng.toString())
                    .openOn(mymap);
            }
            mymap.on('click', onMapClick);        
        }
        </script>
    </head>
    <body onload="init();">
        <div id="mapBasic" style="height:100%;"></div>
    </body>
</html>